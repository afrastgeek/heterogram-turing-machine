# Heterogram Turing Machine

> Heterogram Checker using Turing Machine in JavaScript

http://afrastgeek.gitlab.io/heterogram-turing-machine

Based on Fabian Vogler and Cyril Gabathuler work at
[Turing](https://github.com/fabian/Turing) Project.
